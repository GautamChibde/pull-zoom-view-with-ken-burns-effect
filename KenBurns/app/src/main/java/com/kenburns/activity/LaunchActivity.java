package com.kenburns.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;

import com.gts.ui.view.activity.AbstractActivity;
import com.kenburns.R;
import com.kenburns.domain.StaticData;

public class LaunchActivity extends AbstractActivity {
    private LoadImage task;
    private boolean taskStatus = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        task = new LoadImage();
        ImageView i = (ImageView) findViewById(R.id.image);
        assert i != null;
        i.setImageResource(R.drawable.splash);
        if (taskStatus) {
            taskStatus = false;
            task.execute();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_launch;
    }

    private void initView() {
        if (!taskStatus) {
            task.cancel(true);
        }
        startNewActivity(HomeActivity.class);
        finish();
    }

    private class LoadImage extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            StaticData.create();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            initView();
        }
    }
}

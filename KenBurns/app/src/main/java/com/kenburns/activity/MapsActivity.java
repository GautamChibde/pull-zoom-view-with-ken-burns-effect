package com.kenburns.activity;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gts.ui.view.activity.AbstractActivity;
import com.gts.util.Constant;
import com.gts.util.PlatformUtil;
import com.kenburns.R;
import com.kenburns.domain.model.People;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MapsActivity extends AbstractActivity implements OnMapReadyCallback {

    private People people;
    private Bitmap image;
    private static GoogleMap gMap;
    private boolean loadingComplete = true;
    private LoadImageIntoBitMap task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        people = (People) getIntent().getSerializableExtra(Constant.MODEL);
        task = new LoadImageIntoBitMap();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        if (loadingComplete) {
            loadingComplete = false;
            task.execute();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void maps(GoogleMap googleMap) {
        task.cancel(true);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLng marker = new LatLng(people.getLoc().getLatitude(), people.getLoc().getLongitude());
        googleMap.addMarker(new MarkerOptions()
                .position(marker)
                .title(people.getLoc().getName())
                .icon(BitmapDescriptorFactory.fromBitmap(image))
                .anchor(0.5f, 1));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker, 13));
    }

    private class LoadImageIntoBitMap extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            final ImageLoader imageLoader = ImageLoader.getInstance();
            image = PlatformUtil.getRoundedBitmap(
                    imageLoader.loadImageSync(people.getDetails().getImage()));
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            maps(gMap);
        }
    }
}

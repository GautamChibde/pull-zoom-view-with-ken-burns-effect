package com.kenburns.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gts.util.Constant;
import com.gts.util.PlatformUtil;
import com.kenburns.R;
import com.kenburns.activity.MapsActivity;
import com.kenburns.domain.model.People;

import java.util.List;

public class ListItemAdapter extends ArrayAdapter<People> {
    private final LayoutInflater inflater;
    private List<People> items;

    public ListItemAdapter(Context context, List<People> items) {
        super(context, 0, items);
        inflater = LayoutInflater.from(context);
        this.items = items;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v;
        if (convertView == null) {
            v = inflater.inflate(R.layout.item_list, parent, false);
        } else {
            v = convertView;
        }
        ImageView image = (ImageView) v.findViewById(R.id.iv_image);
        TextView name = (TextView) v.findViewById(R.id.tv_name);
        TextView serial = (TextView) v.findViewById(R.id.tv_serial);
        TextView location = (TextView) v.findViewById(R.id.tv_address);
        ImageView loc = (ImageView) v.findViewById(R.id.iv_location);
        location.setText(items.get(position).getLoc().getName());
        serial.setText(items.get(position).getDetails().getSerialNumber());

        loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), MapsActivity.class);
                i.putExtras(PlatformUtil.bundleSerializable(Constant.MODEL, items.get(position)));
                getContext().startActivity(i);
            }
        });

        PlatformUtil.setImagePicasso(
                getContext(),
                items.get(position).getDetails().getImage(),
                image,
                R.drawable.above_shadow,
                R.drawable.loading);
        name.setText(items.get(position).getName());
        return v;
    }
}
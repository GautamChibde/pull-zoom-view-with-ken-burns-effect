package com.kenburns;

import com.gts.infra.App;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class TestApp extends App {
    @Override
    public void onCreate() {
        super.onCreate();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheExtraOptions(100, 100)
                .build();
        ImageLoader.getInstance().init(config);
        init(true);
    }
}

package com.kenburns.domain.model;

import com.gts.infra.orm.SugarRecord;
import com.gts.infra.orm.dsl.Column;
import com.gts.infra.orm.dsl.Table;
import com.gts.infra.orm.repo.BaseRepo2;

import java.io.Serializable;
import java.util.List;

@Table
public class People implements Serializable {

    private static final Long serialVersionUID = 1L;

    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "detail")
    private PeopleDetails details;

    @Column(name = "loc_id")
    private GpsLocation loc;

    public People() {
    }

    public void save(){
        SugarRecord.save(this);
    }

    public People(Long id, String name, GpsLocation loc) {
        this.id = id;
        this.name = name;
        this.loc = loc;
    }

    public People(Long id, String name, PeopleDetails details, GpsLocation loc) {
        this.id = id;
        this.name = name;
        this.details = details;
        this.loc = loc;
    }

    public People(String name, GpsLocation loc) {
        this.name = name;
        this.loc = loc;
    }

    public static List<People> getAll(){
        return BaseRepo2.getAll(People.class);
    }

    @Override
    public String toString() {
        return "People{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", details=" + details +
                ", loc=" + loc +
                '}';
    }

    public PeopleDetails getDetails() {
        return details;
    }

    public void setDetails(PeopleDetails details) {
        this.details = details;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GpsLocation getLoc() {
        return loc;
    }

    public void setLoc(GpsLocation loc) {
        this.loc = loc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

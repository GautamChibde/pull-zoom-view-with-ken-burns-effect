package com.kenburns.domain.model;

import com.gts.infra.orm.SugarRecord;
import com.gts.infra.orm.dsl.Column;
import com.gts.infra.orm.dsl.Table;

import java.io.Serializable;
import java.util.Date;

@Table
public class PeopleDetails implements Serializable {

    private static final Long serialVersionUID = 1L;

    private Long id;

    @Column(name = "serial_number")
    private String serialNumber;

    @Column(name = "date")
    private Date date;

    @Column(name = "image")
    private String image;

    public PeopleDetails() {
    }

    public PeopleDetails(Long id, String serialNumber, String image) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.image = image;
    }

    public PeopleDetails(Long id, Date date, String image, String serialNumber) {
        this.id = id;
        this.date = date;
        this.image = image;
        this.serialNumber = serialNumber;
    }

    @Override
    public String toString() {
        return "PeopleDetails{" +
                "id=" + id +
                ", serialNumber='" + serialNumber + '\'' +
                ", date=" + date +
                ", image='" + image + '\'' +
                '}';
    }
    public void save(){
        SugarRecord.save(this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

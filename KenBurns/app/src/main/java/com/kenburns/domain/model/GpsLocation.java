package com.kenburns.domain.model;

import com.gts.infra.orm.SugarRecord;
import com.gts.infra.orm.dsl.Column;
import com.gts.infra.orm.dsl.Table;

import java.io.Serializable;

@Table
public class GpsLocation implements Serializable {

    private static final Long serialVersionUID = 1L;

    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "lat")
    private double latitude;

    @Column(name = "lang")
    private double longitude;

    public GpsLocation() {
    }

    public GpsLocation(Long id, String name, double latitude, double longitude) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }


    public double getLongitude() {
        return longitude;
    }

    public void save() {
        SugarRecord.save(this);
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "GpsLocation{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

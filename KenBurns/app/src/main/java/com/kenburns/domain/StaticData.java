package com.kenburns.domain;

import com.kenburns.domain.model.GpsLocation;
import com.kenburns.domain.model.People;
import com.kenburns.domain.model.PeopleDetails;

import java.util.Date;

public class StaticData {

    public static final String ICON = "http://earthtones.org/wp-content/uploads/2012/10/icon.png";

    public static void create() {
        GpsLocation ewl = new GpsLocation(1L, "Brooklyn", 40.6782, -73.9442);
        ewl.save();
        GpsLocation bpl = new GpsLocation(2L, "Shawnee Oklahoma", 35.3273, -96.9253);
        bpl.save();
        GpsLocation ncl = new GpsLocation(3L, "long beach california", 33.7701, -118.1937);
        ncl.save();
        GpsLocation csl = new GpsLocation(4L, "New York City", 40.7128, -74.0059);
        csl.save();
        GpsLocation jll = new GpsLocation(5L, "Indian Hills Kentucky", 38.2726, -85.6627);
        jll.save();
        GpsLocation mfl = new GpsLocation(6L, "Memphis Tennessee", 35.1495, -90.0490);
        mfl.save();
        GpsLocation mpl = new GpsLocation(7L, "Sherman Oaks", 34.1490, -118.4514);
        mpl.save();
        GpsLocation bol = new GpsLocation(8L, "Hawaii", 19.8968, -155.5828);
        bol.save();
        GpsLocation stl = new GpsLocation(9L, "Mumbai", 19.0760, 72.8777);
        stl.save();
        GpsLocation thl = new GpsLocation(10L, "Syracuse", 43.048, -76.1474);
        thl.save();
        GpsLocation wwl = new GpsLocation(11L, "Canoga Park", 34.2083, -118.6059);
        wwl.save();
        GpsLocation ahl = new GpsLocation(12L, "Chicago", 41.8781, -87.6298);
        ahl.save();

        PeopleDetails ew = new PeopleDetails(1L, "frinfedqtkf6", "http://res.cloudinary.com/dvkxfgprc/image/upload/v1473436920/emma_alkcw5.jpg");
        ew.save();
        PeopleDetails bp = new PeopleDetails(2L, "8926m7oow7pw", "http://res.cloudinary.com/dvkxfgprc/image/upload/v1473436907/brad_vypplu.jpg");
        bp.save();
        PeopleDetails nc = new PeopleDetails(3L, "faz63rsqr84t", "http://res.cloudinary.com/dvkxfgprc/image/upload/v1473436905/cage_kyrmyn.jpg");
        nc.save();
        PeopleDetails cs = new PeopleDetails(4L, "gniriyrcearm", "http://res.cloudinary.com/dvkxfgprc/image/upload/v1473436914/charle_ispzup.jpg");
        cs.save();
        PeopleDetails jl = new PeopleDetails(5L, "spfhpxks9tye", "http://res.cloudinary.com/dvkxfgprc/image/upload/v1473436932/JenniferLawrence_wqjsng.jpg");
        jl.save();
        PeopleDetails mf = new PeopleDetails(6L, "4ns8v44nar95", "http://res.cloudinary.com/dvkxfgprc/image/upload/v1473436932/morgan-freeman_u4hlzs.jpg");
        mf.save();
        PeopleDetails mp = new PeopleDetails(7L, "zczfhjmw5xv6", "http://res.cloudinary.com/dvkxfgprc/image/upload/v1473436920/morph-pedro_aydfey.jpg");
        mp.save();
        PeopleDetails bo = new PeopleDetails(8L, "h8v6g54q8mrh", "http://res.cloudinary.com/dvkxfgprc/image/upload/v1473436914/obama_ki0jco.jpg");
        bo.save();
        PeopleDetails st = new PeopleDetails(9L, "yzi5idf4c2ha", "http://res.cloudinary.com/dvkxfgprc/image/upload/v1473436916/sachine_ek9mkg.jpg");
        st.save();
        PeopleDetails th = new PeopleDetails(10L, "nh2sg6zp5tde", "http://res.cloudinary.com/dvkxfgprc/image/upload/v1473436922/tom_jeif3k.jpg");
        th.save();
        PeopleDetails ww = new PeopleDetails(11L, "zsqprpgo24bn", "http://res.cloudinary.com/dvkxfgprc/image/upload/v1473436927/walt_yfonp0.jpg");
        ww.save();
        PeopleDetails ah = new PeopleDetails(12L, "fpu24x3enev6", "http://res.cloudinary.com/dvkxfgprc/image/upload/v1473436908/cat_ncimwq.jpg");
        ah.save();

        new People(1L, "Emma watson", ew, ewl).save();
        new People(2L, "Anne Hathaway", ah, ahl).save();
        new People(3L, "Brad pitt", bp, bpl).save();
        new People(4L, "Nicolas Cage", nc, ncl).save();
        new People(5L, "Charlie Sheen", cs, csl).save();
        new People(6L, "Jennifer Lawrence", jl, jll).save();
        new People(7L, "Morgan Freeman", mf, mfl).save();
        new People(8L, "Marco polo", mp, mpl).save();
        new People(9L, "Barack Obama", bo, bol).save();
        new People(10L, "Sachin Tendulkar", st, stl).save();
        new People(11L, "Tom Henry", th, thl).save();
        new People(12L, "Walter White", ww, wwl).save();
    }
}

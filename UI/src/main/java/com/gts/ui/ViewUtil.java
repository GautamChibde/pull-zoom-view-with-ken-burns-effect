package com.gts.ui;

import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ViewUtil {

    public static boolean isAnyEmpty(EditText... views) {
        for (EditText textView : views) {
            if (textView != null && isEmpty(textView)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isEmpty(EditText textView) {
        if (TextUtils.isEmpty(textView.getText())) {
            textView.setError(
                    ((TextInputLayout) textView.getParent())
                            .getHint() + " is Required"
            );
            textView.requestFocus();
            return true;
        } else {
            textView.setError(null);
            return false;
        }
    }

    public static boolean isAnyChanged(Object[] orig, EditText... views) {
        if (orig.length != views.length) {
            throw new RuntimeException("Values should equal views");
        }
        for (int i = 0; i < orig.length; i++) {
            if (views[i] != null && isChanged(orig[i], views[i])) {
                return true;
            }
        }
        return false;
    }

    public static boolean isChanged(Object original, EditText editText) {
        return !original.equals(editText.getText().toString());
    }

    public static void setTextViewText(View parent, int tvId, Object data) {
        ((TextView) parent.findViewById(tvId)).setText(String.valueOf(data));
    }

    private ViewUtil() {
        throw new IllegalStateException("Utility class. Do not instantiate");
    }
}

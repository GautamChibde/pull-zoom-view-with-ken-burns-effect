package com.gts.ui;

//import com.gts.domain.ec.service.order.OrderService;

import com.gts.infra.App;

@SuppressWarnings("unused")
public abstract class OrderApp extends App {
    private boolean enablePlaceOrder = false;

    public boolean isEnablePlaceOrder() {
        return enablePlaceOrder;
    }

    public void setEnablePlaceOrder(boolean enablePlaceOrder) {
        this.enablePlaceOrder = enablePlaceOrder;
    }

    //protected OrderService orderService = new OrderService(this);

    //public OrderService getOrderService() {
    //return orderService;
    //}
}

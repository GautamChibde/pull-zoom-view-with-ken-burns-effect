package com.gts.ui.view.component.control.view;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.gts.infra.orm.base.Model;
import com.gts.ui.R;

public abstract class ReadOnlyText<T extends Model>
        extends TextView implements ModelView<T> {

    private T model;

    public ReadOnlyText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ReadOnlyText(Context context) {
        super(context);
    }

    public ReadOnlyText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setCompoundDrawablesWithIntrinsicBounds(
                0, 0, R.drawable.ic_edit_black_24dp, 0
        );
        setTextSize(18f);
        setTextColor(Color.BLACK);
        setGravity(Gravity.CENTER_VERTICAL);
        setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (event.getRawX() >= (getRight() - 60)) {
                        onClick();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
        if (model != null) {
            setText(model.getLabel());
        }
    }

    protected abstract void onClick();

}

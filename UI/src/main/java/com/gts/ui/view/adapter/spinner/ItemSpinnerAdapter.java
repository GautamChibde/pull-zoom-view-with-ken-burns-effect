package com.gts.ui.view.adapter.spinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gts.ui.R;
import com.gts.ui.view.model.Link;

import java.util.List;

public class ItemSpinnerAdapter extends ArrayAdapter<Link> {

    protected final LayoutInflater mInflater;

    public ItemSpinnerAdapter(Context context, List<Link> objects) {
        super(context, 0, objects);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View collapsedView;
        Link link = getItem(position);
        if (convertView == null) {
            collapsedView = mInflater.inflate(R.layout.layout_imageview, parent, false);
        } else {
            collapsedView = convertView;
        }
        ImageView linkImage = (ImageView) collapsedView.findViewById(R.id.ivIcon);
        linkImage.setBackgroundResource(getItem(position).getImageId());
        return collapsedView;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View collapsedView;
        Link link = getItem(position);
        if (convertView == null) {
            collapsedView = mInflater.inflate(R.layout.layout_image_text, parent, false);
        } else {
            collapsedView = convertView;
        }
        TextView textView = (TextView) collapsedView.findViewById(R.id.tvLabel);
        textView.setText(link.getName());
        ImageView linkImage = (ImageView) collapsedView.findViewById(R.id.ivIcon);
        linkImage.setBackgroundResource(getItem(position).getImageId());
        return collapsedView;
    }


}

package com.gts.ui.view.activity.home;

import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;

import com.gts.ui.R;

public abstract class TabbedNoDrawerHomeActivity extends AbstractHomeActivity {

    @Override
    protected void initView() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        if (pager != null) {
            pager.setAdapter(getPagerAdapter());
            if (tabLayout != null) {
                tabLayout.setupWithViewPager(pager);
            }
        }
    }

    protected abstract PagerAdapter getPagerAdapter();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_tabbed_home;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (addSearchMenu()) {
            getMenuInflater().inflate(R.menu.tabbed_menu, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    protected boolean addSearchMenu() {
        return false;
    }

}

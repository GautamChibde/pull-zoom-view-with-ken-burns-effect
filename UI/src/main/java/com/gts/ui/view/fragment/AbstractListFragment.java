package com.gts.ui.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.gts.ui.view.ViewFactory;
import com.gts.ui.view.adapter.list.AbstractListAdapter;

public abstract class AbstractListFragment<T> extends AbstractFragment {

    protected AbstractListAdapter<T> adapter;

    protected ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        listView = ViewFactory.getListView(
                getActivity(),
                adapter = getAdapter(),
                getListener()
        );
        customizeView(listView);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        return listView;
    }

    protected void customizeView(ListView listView) {

    }

    protected abstract AdapterView.OnItemClickListener getListener();

    protected abstract AbstractListAdapter<T> getAdapter();
}

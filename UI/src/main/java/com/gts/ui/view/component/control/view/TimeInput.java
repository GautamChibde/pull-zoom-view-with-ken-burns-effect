package com.gts.ui.view.component.control.view;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.TimePicker;

import com.gts.infra.orm.base.Model;
import com.gts.ui.view.component.control.listener.ActionListener;
import com.gts.util.Constant;

import java.util.Calendar;

public class TimeInput<T extends Model.TimeModel>
        extends DialogFragment implements ModelView<T> {

    private T model;
    private ActionListener<Model.TimeModel> listener;
    private TimePickerDialog pickerDialog;

    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pickerDialog = new TimePickerDialog(
                getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (listener != null) {
                            Calendar selectedDateCal = Calendar.getInstance();
                            selectedDateCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            selectedDateCal.set(Calendar.MINUTE, minute);
                            listener.onSubmit(new Model.TimeModel(selectedDateCal.getTime()));
                        } else {
                            Log.w("TimeInput", "Set a listener argument with key PICKER_LISTENER ");
                        }
                    }
                },
                0, 0, false
        );
        initControl();
    }

    @SuppressWarnings("unchecked")
    void initControl() {
        if (getArguments() == null) {
            throw new RuntimeException("Args cannot be null");
        }
        listener = (ActionListener<Model.TimeModel>)
                getArguments().getSerializable(Constant.PICKER_LISTENER);
        model = (T) getArguments().getSerializable(Constant.MODEL);
        initPicker();
    }

    private void initPicker() {
        setModel(model);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        initControl();
        return pickerDialog;
    }

    @Override
    public void onStart() {
        initControl();
        super.onStart();
    }

    @Override
    public void setModel(T model) {
        this.model = model;
        if (pickerDialog != null) {
            if (model.getValue() == null) {
                Calendar now = Calendar.getInstance();
                pickerDialog.updateTime(
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE)
                );
            } else {
                pickerDialog.updateTime(
                        model.getField(Calendar.HOUR_OF_DAY),
                        model.getField(Calendar.MINUTE)
                );
            }
        }
    }


    @Override
    public T getModel() {
        return model;
    }

}

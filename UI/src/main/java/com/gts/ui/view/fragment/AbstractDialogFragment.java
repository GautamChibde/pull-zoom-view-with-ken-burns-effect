package com.gts.ui.view.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.gts.infra.App;
import com.gts.ui.R;
import com.gts.infra.event.LoadViewEvent;
import com.gts.ui.view.activity.AppActivity;
import com.gts.ui.view.controller.UpPanelManager;
import com.gts.util.PlatformUtil;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

@SuppressWarnings("unused")
public class AbstractDialogFragment extends DialogFragment {

    private TextView tvTitle;
    private boolean shown = false;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getLayoutId() != 0) {
            View root = inflater.inflate(getLayoutId(), container, false);
            initToolbar(root);
            initView(root);
            return root;
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void initToolbar(View root) {
        Toolbar toolbar = (Toolbar) root.findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setNavigationIcon(
                    ContextCompat.getDrawable(
                            getActivity(),
                            R.drawable.ic_arrow_back_white_24dp
                    )
            );
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    close();
                }
            });
            if (getMenu() != 0) {
                toolbar.inflateMenu(getMenu());
                toolbar.setOnMenuItemClickListener(getMenuClickListener());
            }
            tvTitle = (TextView) toolbar.findViewById(R.id.tvTitle);
            if (tvTitle != null) {
                tvTitle.setText(getTitle());
            }
        }
    }

    private String getTitle() {
        return "";
    }

    private Toolbar.OnMenuItemClickListener getMenuClickListener() {
        return new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        };
    }

    private int getMenu() {
        return 0;
    }


    @Override
    public void show(FragmentManager manager, String tag) {
        if (shown) return;
        super.show(manager, tag);
        shown = true;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        shown = false;
        super.onDismiss(dialog);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!(getActivity() instanceof AppActivity)) {
            throw new RuntimeException("AbstractDialogFragment fragment can only be used " +
                    "with a host of type AbstractActivity");
        }
    }

    protected int getLayoutId() {
        return 0;
    }

    protected void initView(View root) {
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            );
            // dialog.getWindow().setSoftInputMode(
            //         WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        if (getDialogAnimation() != 0) {
            dialog.getWindow().getAttributes().windowAnimations = getDialogAnimation();
        }
//        WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
//        layoutParams.gravity = Gravity.TOP | Gravity.START;
//        layoutParams.x = 0;
//        layoutParams.y = 0;

        dialog.show();
        return dialog;
    }

    public void setTitle(CharSequence title) {
        tvTitle.setText(title);
    }

    protected void close() {
        if (shown) {
            dismiss();
        } else {
            PlatformUtil.hideKeyboard(getActivity(), tvTitle);
            post(
                    new UpPanelManager.TogglePanelEvent(
                            UpPanelManager.STATE.HIDDEN
                    )
            );

        }
    }

    @Override
    public void onDestroy() {
        getApp().unRegisterListener(this);
        super.onDestroy();
    }

    protected int getDialogAnimation() {
        return 0;
    }

    protected void registerAsListener() {
        getApp().registerListener(this);
    }


    protected static void post(Object event) {
        EventBus.getDefault().post(event);
    }

    public App getApp() {
        return ((AppActivity) getActivity()).getApp();
    }

    protected void replaceWithFragment(Class<? extends Fragment> fragmentClass,
                                       Bundle data, boolean addToBackStack) {
        EventBus.getDefault().post(new LoadViewEvent(fragmentClass, data, addToBackStack));
    }

    protected void replaceWithFragment(Class<? extends Fragment> fragmentClass) {
        replaceWithFragment(fragmentClass, new Bundle(), false);
    }

    protected void showDialog(DialogFragment fragment, Bundle dataBundle, String tag) {
        fragment.setArguments(dataBundle);
        fragment.show(getActivity().getFragmentManager(), tag);
    }

    protected void showActivity(Class<? extends Activity> activityClass, Bundle data) {
        EventBus.getDefault().post(new LoadViewEvent(activityClass, data));
    }

    protected void showActivity(Class<? extends Activity> activityClass) {
        showActivity(activityClass, new Bundle());
    }

    protected void hideKeyboard() {
        PlatformUtil.hideKeyboard(getActivity());
    }

    protected void askPermission(int requestId, String... permissions) {
        List<String> needPerms = new ArrayList<>();
        int[] grantResults = new int[permissions.length];
        for (int i = 0; i < permissions.length; i++) {
            if (permissions[i] != null) {
                if (!hasPermission(permissions[i])) {
                    needPerms.add(permissions[i]);
                } else {
                    grantResults[i] = PackageManager.PERMISSION_GRANTED;
                }
            }
        }
        if (needPerms.size() > 0) {
            FragmentCompat.requestPermissions(this,
                    needPerms.toArray(new String[needPerms.size()]), requestId);
        } else {
            onRequestPermissionsResult(requestId, permissions, grantResults);
        }
    }

    public void onRequestPermissionsResult(int requestId,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
    }

    protected boolean hasPermission(String permission) {
        return !TextUtils.isEmpty(permission) &&
                PackageManager.PERMISSION_GRANTED ==
                        ContextCompat.checkSelfPermission(getActivity(), permission);
    }


}

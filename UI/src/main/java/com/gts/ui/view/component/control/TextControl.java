package com.gts.ui.view.component.control;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.gts.infra.orm.base.Model;
import com.gts.ui.view.component.control.view.SuggestiveTextInput;
import com.gts.util.PlatformUtil;

public class TextControl<T extends Model> extends BaseControl<T> {

    private SuggestiveTextInput<T> editView;

    public TextControl(Context context) {
        super(context);
    }

    public TextControl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected View getEditView() {
        return editView =
                new SuggestiveTextInput<>(getContext(), new InputActionListener() {
                    @Override
                    public void onClose() {
                        super.onClose();
                        editView.getText().clear();
                    }
                });
    }

    @Override
    protected void onEditClick() {
        super.onEditClick();
        editView.requestFocus();
        editView.setCursorVisible(true);
        PlatformUtil.showKeyboard(getContext());
    }

}

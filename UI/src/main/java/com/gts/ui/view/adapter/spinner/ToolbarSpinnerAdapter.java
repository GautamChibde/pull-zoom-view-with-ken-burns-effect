package com.gts.ui.view.adapter.spinner;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ToolbarSpinnerAdapter extends AbstractSpinnerAdapter<String> {

    private final int backgroundColour;
    private final int textColour;

    public ToolbarSpinnerAdapter(Context context, List<String> objects,
                                 int backgroundColour, int textColour) {
        super(context, objects);
        this.backgroundColour = backgroundColour;
        this.textColour = textColour;
    }

    private View inflateView(ViewGroup parent) {
        return mInflater.inflate(android.R.layout.simple_list_item_1, parent, false);
    }

    @Override
    protected void initDropDownView(View root, int position) {
        TextView view = (TextView) root;
        view.setText(getDropDownText(position));
        view.setBackgroundColor(context.getResources().getColor(backgroundColour));
        view.setTextColor(context.getResources().getColor(textColour));
    }

    protected String getDropDownText(int position) {
        return getItem(position);
    }

    protected String getText(int position) {
        return getItem(position);
    }

    @Override
    protected View getDropDownView(int position, ViewGroup parent) {
        return inflateView(parent);
    }

    @Override
    protected void initView(View root, int position) {
        TextView view = (TextView) root;
        view.setText(getText(position));
        view.setBackgroundColor(context.getResources().getColor(backgroundColour));
        view.setTextColor(context.getResources().getColor(textColour));
    }

    @Override
    protected View getView(int position, ViewGroup parent) {
        return inflateView(parent);
    }
}

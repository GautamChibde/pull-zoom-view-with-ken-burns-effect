package com.gts.ui.view.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.TimePicker;

import com.gts.util.Constant;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class TimePickerFragment extends DialogFragment {

    private TimeSelectListener listener;
    private int hour, min;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listener = (TimeSelectListener)
                getArguments().getSerializable(Constant.PICKER_LISTENER);
        hour = getArguments().getInt(Constant.HOUR);
        min = getArguments().getInt(Constant.MINUTE);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new TimePickerDialog(
                getActivity(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (listener != null) {
                            Calendar selectedDateCal = Calendar.getInstance();
                            selectedDateCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            selectedDateCal.set(Calendar.MINUTE, minute);
                            listener.onTimeSet(selectedDateCal.getTime());
                        } else {
                            Log.w("TimePickerFragment",
                                    "Set a listener with key PICKER_LISTENER argument");
                        }
                    }
                },
                hour,
                min,
                true
        );
    }

    public interface TimeSelectListener extends Serializable {

        void onTimeSet(Date selectedDate);
    }
}

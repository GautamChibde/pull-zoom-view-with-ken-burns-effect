package com.gts.ui.view.adapter.spinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

public abstract class AbstractSpinnerAdapter<T> extends ArrayAdapter<T> {

    protected final LayoutInflater mInflater;
    protected final Context context;

    public AbstractSpinnerAdapter(Context context, List<T> objects) {
        super(context, 0, objects);
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = getView(position, parent);
        }
        initView(convertView, position);
        return convertView;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = getDropDownView(position, parent);
        }
        initDropDownView(convertView, position);
        return convertView;
    }

    protected abstract void initDropDownView(View root, int position);

    protected abstract View getDropDownView(int position, ViewGroup parent);

    protected abstract void initView(View root, int position);

    protected abstract View getView(int position, ViewGroup parent);
}

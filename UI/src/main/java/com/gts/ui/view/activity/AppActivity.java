package com.gts.ui.view.activity;


import com.gts.infra.App;

public interface AppActivity {

    App getApp();

    void askPermission(int requestId, String... permissions);

}

package com.gts.ui.view.adapter.recycler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gts.ui.R;

import de.greenrobot.event.EventBus;

public class ViewHolder<T> extends RecyclerView.ViewHolder {

    protected Context context;
    protected ImageView ivDelete;

    public ViewHolder(View v, Context context) {
        super(v);
        ivDelete = (ImageView) v.findViewById(R.id.ivDelete);
        this.context = context;
    }

    @CallSuper
    public void initView(final T item) {
        if (ivDelete != null) {
            ivDelete.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteClicked(item);
                        }
                    }
            );
        }
        itemView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        detailClicked(item);
                    }
                }
        );
    }

    protected void startNewActivity(Class<? extends Activity> clazz, Bundle data) {
        Intent intent = new Intent(context, clazz);
        intent.putExtras(data);
        context.startActivity(intent);
    }

    protected void detailClicked(T item) {
        post(new AbstractRecyclerAdapter.ItemSelectedEvent<>(item));
    }

    protected void deleteClicked(T item) {
        post(new AbstractRecyclerAdapter.DeleteClickedEvent<>(item));
    }

    protected static void post(Object event) {
        EventBus.getDefault().post(event);
    }
}

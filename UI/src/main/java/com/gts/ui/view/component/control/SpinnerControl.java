package com.gts.ui.view.component.control;

import android.content.Context;
import android.util.AttributeSet;

import com.gts.infra.orm.base.Model;
import com.gts.ui.view.component.control.view.SpinnerInput;

import java.util.ArrayList;
import java.util.List;

public class SpinnerControl<T extends Model> extends BaseControl<T> {

    protected SpinnerInput<T> editView;
    private List<T> items = new ArrayList<>();

    public SpinnerControl(Context context) {
        super(context);
    }

    public SpinnerControl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public SpinnerInput<T> getEditView() {
        return editView = new SpinnerInput<>(
                getContext(), items, new InputActionListener()
        );
    }

    @Override
    protected void onEditClick() {
        super.onEditClick();
        editView.setModel(readView.getModel());
        editView.performClick();
    }

    public void setItems(List<T> items){
        this.items = items;
    }

    public void notifyDataSetChanged(){
        editView.notifyDataSetChanged();
    }

}

package com.gts.ui.view.fragment.drawer;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;

import com.gts.ui.view.fragment.AbstractFragment;
import com.gts.ui.view.model.Link;

import de.greenrobot.event.EventBus;

/**
 * Attach this to any activity with a drawer layout.
 * Post a SetupDrawerEvent with the fragmentId and DrawerLayoutId when
 * you need to attach this fragment.
 * Override onCreateView to create the drawer view. Initialize all dynamic content in initView
 **/
public abstract class NavigationDrawer extends AbstractFragment {

    private ActionBarDrawerToggle mDrawerToggle;
    protected DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;
    protected View root;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle != null) {
            return mDrawerToggle.onOptionsItemSelected(item)
                    || super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @SuppressWarnings("unused")
    public void onEvent(SetupDrawerEvent event) {
        if (getActivity() != null) {
            mFragmentContainerView = getActivity().findViewById(event.getFragmentId());
            mDrawerLayout = event.getDrawerLayout();
            mDrawerToggle = getActionBarDrawerToggle();
            mDrawerLayout.addDrawerListener(mDrawerToggle);
            initView(root);
        }
    }

    @SuppressWarnings("unused")
    public void onEvent(SyncDrawerEvent event) {
        mDrawerToggle.syncState();
    }

    protected void drawerItemClicked(Link link) {
        closeDrawer();
        EventBus.getDefault().post(new DrawerItemClickedEvent(link));
    }

    private void closeDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
    }

    protected ActionBarDrawerToggle getActionBarDrawerToggle() {
        return new ActionBarDrawerToggle(
                getActivity(),
                mDrawerLayout,
                getOpenContentDescRes(),
                getCloseContentDescRes()
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }
                getActivity().invalidateOptionsMenu();
            }
        };
    }

    @Override
    protected boolean isEventListener() {
        return true;
    }

    protected int getOpenContentDescRes() {
        return 0;
    }

    protected int getCloseContentDescRes() {
        return 0;
    }

    public static class SetupDrawerEvent {

        private final DrawerLayout drawerLayout;
        private final int fragmentId;

        public SetupDrawerEvent(DrawerLayout drawerLayout, int fragmentId) {
            this.drawerLayout = drawerLayout;
            this.fragmentId = fragmentId;
        }

        public DrawerLayout getDrawerLayout() {
            return drawerLayout;
        }

        public int getFragmentId() {
            return fragmentId;
        }

    }

    public static final class SyncDrawerEvent {
    }

    public static class DrawerItemClickedEvent {
        private final Link link;

        public DrawerItemClickedEvent(Link link) {
            this.link = link;
        }

        public Link getLink() {
            return link;
        }
    }


}

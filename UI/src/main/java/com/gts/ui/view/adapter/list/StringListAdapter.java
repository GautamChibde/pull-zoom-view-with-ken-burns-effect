package com.gts.ui.view.adapter.list;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class StringListAdapter extends AbstractListAdapter<String> {

    private final int backgroundColor;
    private final int textColor;

    public StringListAdapter(Context context, List<String> objects) {
        super(context, objects);
        backgroundColor = Color.WHITE;
        textColor = Color.BLACK;
    }

    public StringListAdapter(Context context, List<String> objects,
                             int backgroundColor, int textColor) {
        super(context, objects);
        this.backgroundColor = backgroundColor;
        this.textColor = textColor;
    }

    @Override
    protected View getView(int position, ViewGroup parent) {
        return inflater.inflate(
                android.R.layout.simple_list_item_1,
                parent,
                false);
    }

    @Override
    protected void initView(View view, int position) {
        TextView tv = (TextView) view;
        tv.setText(getItem(position));
        tv.setBackgroundColor(backgroundColor);
        tv.setTextColor(textColor);
    }
}

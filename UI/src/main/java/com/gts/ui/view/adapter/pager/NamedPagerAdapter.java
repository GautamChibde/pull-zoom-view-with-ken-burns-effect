package com.gts.ui.view.adapter.pager;

import android.app.FragmentManager;

import com.gts.infra.orm.base.NamedRecord;

public abstract class NamedPagerAdapter<T extends NamedRecord>
        extends AbstractPagerAdapter<T> {

    public NamedPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    protected CharSequence getTitle(T t) {
        return t.getName();
    }

}

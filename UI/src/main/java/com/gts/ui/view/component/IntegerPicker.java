package com.gts.ui.view.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.gts.ui.R;
import com.gts.util.TextUtils;

public class IntegerPicker extends EditText {

    private final int min;
    private final int max;
    private final int step;

    public IntegerPicker(Context context) {
        super(context);
        min = 0;
        max = 100;
        step = 1;
    }

    public IntegerPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.IntegerPicker,
                0, 0);
        min = a.getInteger(R.styleable.IntegerPicker_min, 0);
        max = a.getInteger(R.styleable.IntegerPicker_max, 100);
        step = a.getInteger(R.styleable.IntegerPicker_step, 1);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        // if Initial value not set or not within bounds
        if (!isValid(getAsInt())) {
            setText(String.valueOf(min));
        }
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() <= IntegerPicker.this.getLeft() + 100) {
                        add(step);
                        return true;
                    }
                    if (event.getRawX() >= (IntegerPicker.this.getRight() - 60)) {
                        add(-step);
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private boolean isValid(int value) {
        return (value >= min) && (value <= max);
    }

    private int getAsInt() {
        int currentValue = 0;
        String currValue = this.getText().toString();
        if (TextUtils.isDigitsOnly(currValue)) {
            currentValue = Integer.parseInt(currValue);
        }
        return currentValue;
    }

    public void add(int value) {
        int currentValue = getAsInt();
        int nextValue = currentValue + value;
        if (nextValue <= min) {
            setText(String.valueOf(min));
        } else if (nextValue >= max) {
            setText(String.valueOf(max));
        } else {
            setText(String.valueOf(nextValue));
        }
    }

    public int getValue() {
        String currValue = this.getText().toString();
        if (TextUtils.isDigitsOnly(currValue)) {
            return Integer.parseInt(getText().toString());
        } else {
            return min;
        }
    }


}

package com.gts.ui.view.fragment.auth;

import com.gts.ui.R;
import com.gts.ui.ViewUtil;
import com.gts.util.PlatformUtil;


public class SignUpFragment extends AbstractAuthFragment {

    protected int getLayoutId() {
        return R.layout.fragment_signup;
    }

    protected void onSubmit() {
        authManager.signUp(
                etName.getText().toString(),
                etUsername.getText().toString(),
                etPassword.getText().toString()
        );
    }

    protected boolean dataValid() {
        if (ViewUtil.isAnyEmpty(
                etName, etUsername,
                etPassword, etVerifyPassword)
                ) {
            return false;
        }

        if (!PlatformUtil.isValidEmail(etUsername.getText())) {
            etUsername.setError("Email invalid");
            etUsername.requestFocus();
            return false;
        } else {
            etUsername.setError(null);
        }

        if (!etPassword.getText().toString()
                .equals(etVerifyPassword.getText().toString())) {
            etVerifyPassword.setError("Passwords do not match");
            etVerifyPassword.requestFocus();
            return false;
        } else {
            etVerifyPassword.setError(null);
        }
        return true;
    }

    @Override
    protected String getTitle() {
        return "Sign up";
    }
}

package com.gts.ui.view.adapter.list;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gts.ui.view.adapter.spinner.AbstractSpinnerAdapter;

import java.util.List;

public abstract class AbstractSimpleListItemAdapter<T>
        extends AbstractSpinnerAdapter<T> {

    public AbstractSimpleListItemAdapter(Context context, List<T> objects) {
        super(context, objects);
    }

    @Override
    protected void initDropDownView(View root, int position) {
        ((TextView) root).setText(getText(getItem(position)));
    }

    @Override
    protected View getDropDownView(int position, ViewGroup parent) {
        return inflateView(parent);
    }

    private View inflateView(ViewGroup parent) {
        return mInflater.inflate(
                android.R.layout.simple_list_item_1,
                parent,
                false
        );
    }

    @Override
    protected void initView(View root, int position) {
        ((TextView) root).setText(getText(getItem(position)));
    }

    @Override
    protected View getView(int position, ViewGroup parent) {
        return inflateView(parent);
    }

    protected abstract String getText(T item);
}

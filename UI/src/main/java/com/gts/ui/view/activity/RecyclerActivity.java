package com.gts.ui.view.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.gts.ui.R;
import com.gts.ui.view.adapter.recycler.AbstractRecyclerAdapter;

public abstract class RecyclerActivity<T> extends AbstractActivity {

    protected AbstractRecyclerAdapter<T, ?> adapter;
    protected RecyclerView recyclerView;
    protected LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setLayoutManager(layoutManager = new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter = getAdapter());
        if (adapter != null) {
            registerListener(adapter);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_recycler;
    }

    protected abstract AbstractRecyclerAdapter<T, ?> getAdapter();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterListener(adapter);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(DataSetChanged event) {
        if (event.isReload()) {
            adapter.reload();
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @SuppressWarnings("unused")
    public static class DataSetChanged {
        private final boolean reload;

        public DataSetChanged() {
            this.reload = false;
        }

        public DataSetChanged(boolean reload) {
            this.reload = reload;
        }

        public boolean isReload() {
            return reload;
        }
    }

}

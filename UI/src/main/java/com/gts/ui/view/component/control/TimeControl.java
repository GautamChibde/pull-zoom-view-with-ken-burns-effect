package com.gts.ui.view.component.control;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

import com.gts.infra.orm.base.Model;
import com.gts.ui.view.activity.AbstractActivity;
import com.gts.ui.view.component.control.listener.ActionListener;
import com.gts.ui.view.component.control.view.ReadOnlyText;
import com.gts.ui.view.component.control.view.TimeInput;
import com.gts.util.Constant;

public class TimeControl extends BaseControl<Model.TimeModel> {

    private TimeInput<Model.TimeModel> timeInput = new TimeInput<>();

    public TimeControl(Context context) {
        super(context);
    }

    public TimeControl(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    @Override
    protected View getEditView() {
        return new ReadOnlyText<Model.TimeModel>(getContext()) {
            @Override
            protected void onClick() {
            }
        };
    }

    @Override
    protected void onEditClick() {
        Bundle args = new Bundle();
        args.putSerializable(
                Constant.PICKER_LISTENER,
                new ActionListener.SimpleListener<Model.TimeModel>() {
                    @Override
                    public void onSubmit(Model.TimeModel newValue) {
                        if (listener.isValid(newValue)) {
                            listener.onSubmit(newValue);
                            setModel(newValue);
                        }
                    }
                });
        args.putSerializable(Constant.MODEL, getModel());
        ((AbstractActivity) getContext()).showDialog(
                timeInput, args, TimeInput.class.getName()
        );
    }

    @Override
    protected void validationFailed() {
        Toast.makeText(getContext(), "Invalid time", Toast.LENGTH_LONG).show();
    }
}

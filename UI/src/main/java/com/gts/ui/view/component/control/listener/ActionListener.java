package com.gts.ui.view.component.control.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public interface ActionListener<T> extends Serializable {
    void onSubmit(T newValue);

    boolean isValid(T newValue);

    void onClose();

    List<T> onSuggestiveTextInput(String suggestiveInput);

    class SimpleListener<T> implements ActionListener<T> {

        @Override
        public void onSubmit(T newValue) {
        }

        @Override
        public boolean isValid(T newValue) {
            return true;
        }

        @Override
        public void onClose() {
        }

        @Override
        public List<T> onSuggestiveTextInput(String suggestiveInput) {
            return new ArrayList<>();
        }
    }
}
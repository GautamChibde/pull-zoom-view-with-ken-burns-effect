package com.gts.ui.view.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;

import com.gts.infra.orm.base.BaseEntity;
import com.gts.infra.orm.doc.Store;
import com.gts.infra.orm.record.Record;
import com.gts.infra.orm.repo.BaseRepo2;
import com.gts.ui.R;
import com.gts.ui.view.adapter.recycler.AbstractRecyclerAdapter;
import com.gts.ui.view.fragment.ConfirmDialog;
import com.gts.ui.view.listener.InfiniteScrollListener;
import com.gts.util.Constant;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public abstract class InfiniteScrollActivity<T extends Record>
        extends RecyclerActivity<T>
        implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    protected int threshold = 2;
    protected int minCharSearch = 2;

    protected ProgressDialog progress;
    protected boolean isSearchMode = false;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BaseEntity.class.isAssignableFrom(getClazz())) {
            sync((Class<? extends BaseEntity>) getClazz());
        }
        loadNextBatch();
        recyclerView.addOnScrollListener(
                new InfiniteScrollListener(layoutManager, adapter, threshold) {

                    @Override
                    public void loadNextBatch() {
                        if (!InfiniteScrollActivity.this.isSearchMode) {
                            InfiniteScrollActivity.this.loadNextBatch();
                        }
                    }
                }
        );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.recycler_menu, menu);
        initSearchView(menu, R.id.action_search);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.action_add == item.getItemId()) {
            showChangeDialog(null);
        }
        return super.onOptionsItemSelected(item);
    }

    protected void initSearchView(Menu menu, int viewId) {
        final SearchView searchView =
                (SearchView) MenuItemCompat.getActionView(
                        menu.findItem(viewId)
                );
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText != null && newText.length() > minCharSearch) {
            isSearchMode = true;
            progress = new ProgressDialog(this);
            progress.setInverseBackgroundForced(true);
            progress.show();
            new SearchByTextTask().execute(newText);
        } else {
            onClose();
        }
        return false;
    }

    @Override
    public boolean onClose() {
        if (adapter != null) {
            adapter.reload();
        }
        isSearchMode = false;
        return false;
    }

    protected void loadNextBatch() {
        new LoadNextPageTask().execute();
    }

    public void onEvent(Store.RecordSyncedEvent event) {
        if (getClazz().equals(event.getClazz())) {
            adapter.reload();
        }
    }

    public void onEventMainThread(Store.RecordSavedEvent<T> event) {
        if (getClazz().equals(event.getRecord().getClass())) {
            adapter.add(event.getRecord());
            onSaveComplete();
        }
    }

    protected void onSaveComplete() {
    }


    public void onEvent(AbstractRecyclerAdapter.DeleteClickedEvent<T> event) {
        showConfirmDialog(event.getEntity());
    }

    protected void showChangeDialog(T entity) {
        post(new AbstractRecyclerAdapter.ItemSelectedEvent<T>(
                null
        ));
    }

    protected void showConfirmDialog(T entity) {
        Bundle data = new Bundle();
        data.putSerializable(Constant.MESSAGE, getMessage(entity));
        data.putSerializable(Constant.LISTENER, getDeleteActionListener(entity));
        showDialog(
                new ConfirmDialog(),
                data,
                ConfirmDialog.class.getName()
        );
    }

    @NonNull
    protected String getMessage(T entity) {
        return "Confirm delete ?";
    }

    protected ConfirmDialog.ActionListener getDeleteActionListener(T entity) {
        return null;
    }

    protected List<T> searchByText(String query) {
        try {
            return BaseRepo2.searchByName(getClazz(), query);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    protected abstract Class<T> getClazz();

    protected class LoadNextPageTask extends AsyncTask<Void, Void, List<T>> {

        @Override
        protected List<T> doInBackground(Void... params) {
            if (adapter != null) {
                return adapter.getData(adapter.getItemCount() - 1);
            }
            return new ArrayList<>();
        }

        @Override
        protected void onPostExecute(List<T> newBatch) {
            if (newBatch.size() > 0) {
                adapter.addAll(newBatch);
            }
        }
    }

    protected class SearchByTextTask extends AsyncTask<String, Void, List<T>> {

        @Override
        protected List<T> doInBackground(String... params) {
            return searchByText(params[0]);
        }

        @Override
        protected void onPostExecute(List<T> items) {
            adapter.refreshWithSearchResults(items);
            recyclerView.scrollToPosition(0);
            progress.dismiss();
        }
    }
}

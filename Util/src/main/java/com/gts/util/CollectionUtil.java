package com.gts.util;

import java.util.List;

public class CollectionUtil {

    public static String[] listToStringArray(List<?> objects) {
        String[] res = new String[objects.size()];
        for (int i = 0; i < objects.size(); i++) {
            res[i] = String.valueOf(objects.get(i));
        }
        return res;
    }

    private CollectionUtil() {
        throw new IllegalStateException("Utility class. Do not instantiate");
    }

}

package com.gts.util;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressWarnings("unused")
public final class PlatformUtil {

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
    private static final String TAG = "PlatformUtil";

    public static int generateViewId() {
        for (; ; ) {
            final int result = sNextGeneratedId.get();
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1;
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

    public static void setImagePicasso(
            Context context,
            String image,
            final ImageView viewItem,
            final int resource,
            int placeHolder) {
        Picasso.with(context)
                .load(image)
                .placeholder(placeHolder)
                .into(viewItem,
                        new Callback.EmptyCallback() {
                            @Override
                            public void onError() {
                                viewItem.setImageResource(resource);
                            }
                        });
    }

    public static Bundle bundleSerializable(String key, Serializable value) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(key, value);
        return bundle;
    }

    public static Bundle bundleSerializable(Serializable value) {
        if (value != null) {
            return bundleSerializable(
                    value.getClass().getSimpleName(),
                    value
            );
        }
        return new Bundle();
    }

    public static Bitmap getBitmapFromAsset(Context context, String strName) {
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = null;
        try {
            inputStream = assetManager.open(strName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BitmapFactory.decodeStream(inputStream);
    }

    public static void setDefaultFont(Context context,
                                      String staticTypefaceFieldName, String fontAssetName) {
        if (fontAssetName != null) {
            final Typeface regular =
                    Typeface.createFromAsset(
                            context.getAssets(),
                            fontAssetName);
            replaceFont(staticTypefaceFieldName, regular);
        }
    }

    protected static void replaceFont(String staticTypefaceFieldName,
                                      final Typeface newTypeface) {
        try {
            final Field staticField = Typeface.class
                    .getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void displayPromptForEnablingGPS(final Activity activity) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(activity);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String message = "Enable GPS.";

        builder.setMessage(message)
                .setPositiveButton("Location settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                activity.startActivity(new Intent(action));
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });
        builder.create().show();
    }

    public static Location getLastKnownLocation(LocationManager mLocationManager) {
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null
                    || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        if (bestLocation == null) {
            return null;
        }
        return bestLocation;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("deprecation")
    public static void setBackground(View view, Drawable background) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackground(background);
        } else {
            view.setBackgroundDrawable(background);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("deprecation")
    public static void setBackground(Context context, View view, int resId) {
        Drawable background = getDrawable(context, resId);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackground(background);
        } else {
            view.setBackgroundDrawable(background);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressWarnings("deprecation")
    public static Drawable getDrawable(Context context, int resId) {
        return Build.VERSION.SDK_INT >=
                Build.VERSION_CODES.LOLLIPOP ?
                context.getDrawable(resId) :
                context.getResources().getDrawable(resId);
    }

    /**
     * Validation of Phone Number
     */
    public static boolean isValidPhoneNumber(CharSequence target) {
        return target != null
                && !(target.length() < 6 || target.length() > 13)
                && Patterns.PHONE.matcher(target).matches();
    }

    public static boolean isValidEmail(CharSequence target) {
        return target != null &&
                Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    public static void hideKeyboard(Context context, View view) {
        if (view != null && context != null) {
            InputMethodManager imm =
                    (InputMethodManager) context.getSystemService(
                            Context.INPUT_METHOD_SERVICE
                    );
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } else {
            Log.w("PlatformUtil", "Context or view empty");
        }
    }

    public static void hideKeyboard(Activity context) {
        hideKeyboard(context, context.getCurrentFocus());
    }

    public static boolean isNetworkOnline(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void showKeyboard(Context context) {
        InputMethodManager imm =
                (InputMethodManager) context.getSystemService(
                        Context.INPUT_METHOD_SERVICE
                );
        imm.toggleSoftInput(
                InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY
        );
    }


    public static void showKeyboard(Context context, View view) {
        if (view != null && context != null) {
            InputMethodManager imm =
                    (InputMethodManager) context.getSystemService(
                            Context.INPUT_METHOD_SERVICE
                    );
            imm.toggleSoftInput(
                    InputMethodManager.SHOW_FORCED,
                    InputMethodManager.HIDE_IMPLICIT_ONLY
            );
        } else {
            Log.w("PlatformUtil", "Context or view empty");
        }
    }

    public static void animateBackgroundColor(View view, int colorFrom,
                                              int colorTo, int duration) {
        ObjectAnimator.ofObject(view, "backgroundColor",
                new ArgbEvaluator(), colorFrom, colorTo)
                .setDuration(duration)
                .start();
        ObjectAnimator.ofObject(view, "backgroundColor",
                new ArgbEvaluator(), colorTo, colorFrom)
                .setDuration(duration)
                .start();
    }

    public static void animateBackgroundColor(View view, Context context, int colorFrom,
                                              int colorTo, int duration) {
        ObjectAnimator.ofObject(view, "backgroundColor",
                new ArgbEvaluator(), getColor(context, colorFrom), getColor(context, colorTo))
                .setDuration(duration)
                .start();
        ObjectAnimator.ofObject(view, "backgroundColor",
                new ArgbEvaluator(), getColor(context, colorTo), getColor(context, colorFrom))
                .setDuration(duration)
                .start();
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            Log.e("getBitmapFromURL", e.toString());
            return null;
        }
    }

    public static Bitmap getRoundedBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }


    @SuppressWarnings("deprecation")
    public static int getColor(Context context, int colorResId) {
        return context.getResources().getColor(colorResId);
    }

    public static void setTextViewText(View parent, int viewId, String s) {
        ((TextView) parent.findViewById(viewId)).setText(s);
    }


    private PlatformUtil() {
        throw new IllegalStateException("Utility class. Do not instantiate");
    }

}

package com.gts.util;

public class NumberUtil {

    public static boolean isNullOrZero(Long aLong) {
        return aLong == null || aLong == 0;
    }

    private NumberUtil() {
        throw new IllegalStateException("Utility class. Do not instantiate");
    }
}

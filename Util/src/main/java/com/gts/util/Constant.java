package com.gts.util;

import java.text.SimpleDateFormat;
import java.util.Locale;

public interface Constant {


    enum stateType {NEW, ACTIVE, ARCHIVED}

    enum EnquiryProductRol {other, primary}

    enum EnquiryPartyRol {customer, vendor}

    enum EnquiryStatus {DRAFT, COMPLETE, CANCEL}

    String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSSSSS";
    SimpleDateFormat timeFormatter =
            new SimpleDateFormat("HH:mm", Locale.getDefault());
    SimpleDateFormat defaultDateFormatter =
            new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

    String PICKER_LISTENER = "PICKER_LISTENER";
    String MODEL = "MODEL";

    String EVENT = "EVENT";
    String LOCATION = "LOCATION";
    String PARTY = "PARTY";
    String USER = "USER";
    String DAY = "DAY";
    String ENQUIRY = "Enquiry";
    String ORDER = "ORDER";
    String PRODUCT_CATEGORY = "PRODUCT_CATEGORY";
    String PARTY_CATEGORY = "PARTY_CATEGORY";
    String PRODUCT_SLOT = "PRODUCT_SLOT";
    String PERSONAL_DETAILS = "PERSONAL_DETAILS";
    String ADDRESS = "ADDRESS";
    String FORM = "FORM";
    String LIST_TAG = "LIST_FRAGMENT";
    String PRODUCT = "PRODUCT";
    String SUPPLIER = "SUPPLIER";
    String FILTER = "FILTER";
    String FACILITY = "FACILITY";
    String ASSET = "ASSET";
    String ENQUIRY_STATUS = "enquiry_status";
    String PRODUCT_MEDIA = "product_media";

    String KEY_USER_TOKEN = "KEY_USER_TOKEN";
    String KEY_CLIENT_TOKEN = "KEY_CLIENT_TOKEN";
    String KEY_USER_ROL = "KEY_USER_ROL";
    String KEY_USER_ID = "KEY_USER_ID";

    String CONTEXT_SIGNUP = "/api/signup";
    String CONTEXT_TOKEN = "/api/token";

    String GRANT_TYPE_PASSWORD = "password";
    String GRANT_TYPE_CLIENT_CREDENTIALS = "client_credentials";

    String FOR_RESULT = "FOR_RESULT";
    String REQUEST_CODE = "REQUEST_CODE";
    String QUESTION = "QUESTION";
    String TRIP = "TRIP";

    /* Comm_Mech_Type */
    String Type_Mobile = "mobile";
    String Type_Email = "email";
    String Type_Address = "address";

    /* REQUEST ID's */

    int TOKEN_REQUEST_CODE = 101;
    int AUTH_REQUEST_CODE = 102;
    int DATA_SYNC_REQUEST_CODE = 103;
    int PLACE_ORDER_REQUEST_CODE = 104;
    int ORDER_REQUEST_CODE = 105;
    int BID_REQUEST_CODE = 106;

    int PARTY_REQUEST_CODE = 202;
    int LOCATION_REQUEST_CODE = 203;
    int FACILITY_REQUEST_CODE = 204;
    int PRODUCT_REQUEST_CODE = 205;
    int PRODUCT_CATEGORY_REQUEST_CODE = 206;
    int LOOKUP_REQUEST_CODE = 207;
    int ENQUIRY_REQUEST_CODE = 208;

    int PARTY_CATEGORY_REQUEST_CODE = 208;
    int PARTY_SCHEDULE_REQUEST_CODE = 209;
    int CONNECTION_REQUEST_CODE = 210;
    int METER_REQUEST_CODE = 211;
    int PAYMENT_REQUEST_CODE = 212;
    int BILL_REQUEST_CODE = 213;
    int READING_REQUEST_CODE = 214;
    int READ_CYCLE_REQUEST_CODE = 215;
    int PRODUCT_SCHEDULE_REQUEST_CODE = 216;
    int FACILITY_CATEGORY_REQUEST_CODE = 217;
    int ASSET_CATEGORY_REQUEST_CODE = 218;
    int ASSET_REQUEST_CODE = 219;
    int PROVIDER_SCHEDULE_REQUEST_CODE = 220;
    int VEHICLE_REQUEST_CODE = 221;
    int ADMIN_UNIT_REQUEST_CODE = 222;
    int LICENSE_REQUEST_CODE = 223;
    int OFFENCE_REQUEST_CODE = 224;

    /* DOC NAMES */
    String DOC_PARTY = "party";
    String DOC_LOCATION = "location";
    String DOC_PRODUCT = "product";

    String DOC_PRODUCT_CATEGORY = "product_category";
    String DOC_PARTY_CATEGORY = "party_category";
    String DOC_PARTY_SCHEDULE = "party_schedule";
    String DOC_FACILITY_CATEGORY = "facility_category";
    String DOC_FACILITY = "facility";
    String DOC_ASSET_CATEGORY = "asset_category";
    String DOC_ASSET = "asset";
    String DOC_ORDER = "ordr";
    String DOC_BID = "bid";
    String DOC_LOOKUP = "lookup";
    String DOC_CONNECTION = "connection";
    String DOC_ENQUIRY = "enquiry";
    String DOC_METER = "meter";
    String DOC_PAYMENT = "payment";
    String DOC_BILL = "bill";
    String DOC_READING = "reading";
    String DOC_READ_CYCLE = "read_cycle";
    String DOC_ADMIN_UNIT = "admin_unit";
    String DOC_VEHICLE_CATEGORY = "vehicle_category";
    String DOC_VEHICLE = "vehicle";
    String DOC_LICENSE = "license";
    String DOC_OFFENCE = "offence";

    /* RESOURCE URI */
    String SEARCH_RESOURCE_URI = "/api/search";
    String LOOKUP_RESOURCE_URI = "/api/lookup";

    String PARTY_RESOURCE_URI = "/api/party";
    String PARTY_SCHEDULE_RESOURCE_URI = "/api/party_schedule";
    String BID_RESOURCE_URI = "/api/bid";
    String ORDER_RESOURCE_URI = "/api/order";
    String FACILITY_RESOURCE_URI = "/api/facility";
    String ASSET_RESOURCE_URI = "/api/asset";
    String PROVIDER_SCHEDULE_RESOURCE_URI = "/api/provider_schedule";
    String PRODUCT_SCHEDULE_RESOURCE_URI = "/api/product_schedule";
    String VEHICLE_RESOURCE_URI = "/api/vehicle";
    String LICENSE_RESOURCE_URI = "/api/license";
    String OFFENCE_RESOURCE_URI = "/api/offence";
    String PAYMENT_RESOURCE_URI = "/api/payment";
    String ADMIN_UNIT_RESOURCE_URI = "/api/admin_unit";
    String PRODUCT_RESOURCE_URI = "/api/product";
    String ENQUIRY_RESOURCE_URI = "/api/enquiry";

    String CONNECTION_RESOURCE_URI = "/api/connection";
    String BILL_RESOURCE_URI = "/api/bill";
    String READING_RESOURCE_URI = "/api/reading";
    String READ_CYCLE_RESOURCE_URI = "/api/read_cycle";
    String METER_RESOURCE_URI = "/api/meter";

    String LISTENER = "LISTENER";
    String MESSAGE = "MESSAGE";

    String THEME = "THEME";

    String PROVIDER = "PROVIDER";
    String POSITION = "POSITION";
    String PARTY_SCHEDULE = "PARTY_SCHEDULE";
    String PARTY_SCHEDULE_SLOT = "PARTY_SCHEDULE_SLOT";
    String HOUR = "HOUR";
    String MINUTE = "MINUTE";
    String SIGNED_UP = "SIGNED_UP";
    String TITLE = "TITLE";
    String TYPE = "TYPE";

    String REQUEST = "REQUEST";

    String ACCOUNT = "ACCOUNT";
    String CARD_TYPE = "CARD_TYPE";
    String CREDIT = "CREDIT";
    String DEBIT = "DEBIT";

    String LICENCE_NUMBER = "licence_number";
    String VEHICLE_NUMBER = "vehicle_number";

    String LICENCE = "license";
    String VEHICLE = "vehicle";
}

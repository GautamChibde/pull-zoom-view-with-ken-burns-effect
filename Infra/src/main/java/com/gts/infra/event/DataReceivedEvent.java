package com.gts.infra.event;

public class DataReceivedEvent {
    private final Object data;

    public DataReceivedEvent(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }
}

package com.gts.infra.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import de.greenrobot.event.EventBus;

/*
  Dual purpose.

  Acts as a Receiver in which case the
  default constructor and onReceive is called by the runtime
  whenever network state changes

  A global instance is also initialized using the App context
  available via app.getNetworkManager(). This instance can then be
  queried for the network info

 */
public class NetworkManager extends BroadcastReceiver {

    private EventBus bus = EventBus.getDefault();
    private ConnectivityManager cManager;
    private Context context;

    public NetworkManager() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final NetworkInfo nInfo =
                ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE))
                        .getActiveNetworkInfo();
        bus.post(
                new NetworkStateChangedEvent(
                        nInfo != null ? nInfo.getState() : NetworkInfo.State.DISCONNECTED,
                        nInfo != null ? nInfo.getType() : 0
                )
        );
    }

    public NetworkManager(Context context) {
        this.context = context;
    }

    private void initManager() {
        if (cManager == null) {
            cManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
        }
    }

    private NetworkInfo getActiveNetworkInfo() {
        initManager();
        return cManager.getActiveNetworkInfo();
    }

    public boolean isConnected() {
        NetworkInfo netInfo = getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    public boolean isWifi() {
        NetworkInfo netInfo = getActiveNetworkInfo();
        return (netInfo != null
                && netInfo.isConnected()
                && ConnectivityManager.TYPE_WIFI == netInfo.getType());
    }

    public boolean isMobile() {
        NetworkInfo netInfo = getActiveNetworkInfo();
        return (netInfo != null
                && netInfo.isConnected()
                && ConnectivityManager.TYPE_MOBILE == netInfo.getType());
    }


    public static class NetworkStateChangedEvent {
        private final NetworkInfo.State state;
        private final int type;


        public NetworkStateChangedEvent(NetworkInfo.State state, int type) {
            this.state = state;
            this.type = type;
        }

        public boolean isConnected() {
            return NetworkInfo.State.CONNECTED.equals(state);
        }

        public boolean isWifi() {
            return isConnected() &&
                    ConnectivityManager.TYPE_WIFI == type;
        }

        public boolean isMobile() {
            return isConnected() &&
                    ConnectivityManager.TYPE_MOBILE == type;
        }
    }


}

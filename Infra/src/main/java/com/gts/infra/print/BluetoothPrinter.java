package com.gts.infra.print;

import android.bluetooth.BluetoothDevice;

public interface BluetoothPrinter {

    void print(String message);

}

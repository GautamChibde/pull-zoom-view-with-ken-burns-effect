package com.gts.infra.orm.base;

import com.gts.infra.orm.dsl.Column;
import com.gts.infra.orm.repo.BaseRepo2;

public abstract class BaseValueNamed extends BaseValue
        implements NamedRecord, Model<NamedRecord> {

    @Column(name = "name")
    protected String name;

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                '}';
    }

    public void save() {
        BaseRepo2.save(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getLabel() {
        return name;
    }

    @Override
    public NamedRecord getValue() {
        return this;
    }
}

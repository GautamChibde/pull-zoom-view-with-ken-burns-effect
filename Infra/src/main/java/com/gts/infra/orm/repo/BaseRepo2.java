package com.gts.infra.orm.repo;

import com.gts.infra.orm.SugarRecord;
import com.gts.infra.orm.base.BaseEntity;
import com.gts.infra.orm.record.Record;
import com.gts.infra.orm.util.NamingHelper;

import java.util.Date;
import java.util.List;

public class BaseRepo2 {

    public static <T> void save(T row) {
        SugarRecord.save(row);
    }

    public static <T> T findById(Class<T> clazz, long id) {
        return SugarRecord.findById(clazz, id);
    }

    public static <T> List<T> getAll(Class<T> clazz) {
        return SugarRecord.listAll(clazz);
    }

    public static <T> List<T> search(Class<T> clazz, String whereClause, String... args) {
        return SugarRecord.find(clazz, whereClause, args);
    }

    public static <T> List<T> searchWithQuery(Class<T> clazz, String query, String... args) {
        return SugarRecord.findWithQuery(clazz, query, args);
    }

    public static <T> List<T> getActive(Class<T> clazz) {
        return search(clazz, "state = 'active'");
    }

    public static <T> List<T> searchByName(Class<T> clazz, String query) {
        return search(clazz, "name like ? and state = 'active'", "%" + query + "%");
    }

    public static <T extends BaseEntity> Date getLastUpdatedStamp(Class<T> clazz) {
        List<T> lastUpdatedList = searchWithQuery(clazz,
                "select * from " + NamingHelper.toSQLName(clazz) + " order by updated_stamp desc limit 1");
        if (lastUpdatedList.size() == 1) {
            return lastUpdatedList.get(0).getUpdatedStamp();
        }
        return null;
    }

    public static void delete(Record record) {
        SugarRecord.delete(record);
    }

    public static void deleteAll(Class<?> clazz, String whereClause, String... whereArgs) {
        SugarRecord.deleteAll(clazz, whereClause, whereArgs);
    }

}

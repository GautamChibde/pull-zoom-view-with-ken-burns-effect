package com.gts.infra.orm.base;

import com.gts.util.Constant;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public interface Model<T> extends Serializable {

    String getLabel();

    T getValue();

    class StringModel implements Model<String> {

        private String value;

        public StringModel(String value) {
            this.value = value;
        }

        @Override
        public String getLabel() {
            return value;
        }

        @Override
        public String getValue() {
            return value;
        }
    }

    class DateModel implements Model<Date> {
        private Date value;

        public DateModel(Date theDate) {
            this.value = theDate;
        }

        @Override
        public String getLabel() {
            if (value != null) {
                return Constant.defaultDateFormatter.format(value);
            } else {
                return "";
            }
        }

        public Date getValue() {
            return value;
        }

        public Calendar getValueCal() {
            if (value != null) {
                Calendar c = Calendar.getInstance();
                c.setTime(value);
                return c;
            }
            return null;
        }

        public int getField(int field) {
            Calendar c = getValueCal();
            int toReturn = 0;
            if (c != null) {
                c.setTime(value);
                toReturn = c.get(field);
            }
            return toReturn;
        }
    }

    class TimeModel implements Model<Date> {
        private Date value;

        public TimeModel(Date theDate) {
            this.value = theDate;
        }

        @Override
        public String getLabel() {
            if (value != null) {
                return Constant.timeFormatter.format(value);
            } else {
                return "";
            }
        }

        public Date getValue() {
            return value;
        }

        public Calendar getValueCal() {
            if (value != null) {
                Calendar c = Calendar.getInstance();
                c.setTime(value);
                return c;
            }
            return null;
        }

        public int getField(int field) {
            Calendar c = getValueCal();
            int toReturn = 0;
            if (c != null) {
                c.setTime(value);
                toReturn = c.get(field);
            }
            return toReturn;
        }
    }

}

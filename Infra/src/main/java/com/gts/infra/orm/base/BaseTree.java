package com.gts.infra.orm.base;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.gts.infra.orm.dsl.Column;
import com.gts.infra.orm.dsl.Ignore;
import com.gts.infra.orm.repo.TreeRepo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("unused")
public abstract class BaseTree<T extends BaseTree>
        extends BaseValueNamed {

    @Column(name = "path")
    protected String path;

    @Ignore
    protected T parent;

    @Ignore
    protected List<T> children;


    @Override
    @SuppressWarnings("unchecked")
    public void save() {
        if (parent != null) {
            parent.setChildren(null);
        }
        parent = null;
        TreeRepo.save(this);
    }

    public List<T> getNotSelfOrChild() {
        return TreeRepo.searchWithQuery(
                getClazz(),
                "select p.* from product_category as p " +
                        "where path <> ? and path not like ?",
                path,
                path + ".%"
        );
    }

    public List<T> getChildren() {
        if (children == null) {
            synchronized (this) {
                return children = TreeRepo.search(getClazz(), "path like ?", path + ".%");
            }
        }
        return children;
    }

    public T getParent() {
        if (parent == null) {
            synchronized (this) {
                List<T> found = TreeRepo.search(
                        getClazz(),
                        "path = ?", getParentPath(path.split("\\."))
                );
                if (found.size() == 1) {
                    return parent = found.get(0);
                }
            }
        }
        return parent;
    }

    public List<T> getImmediateChildren() {
        List<T> res = new ArrayList<>();
        for (T node : getChildren()) {
            if (node.getPath() != null) {
                if (node.getPath().matches(path + "\\.\\d+")) {
                    res.add(node);
                }
            }
        }
        return res;
    }

    @NonNull
    protected static String getParentPath(String[] tokensChild) {
        if (tokensChild.length > 0) {
            String[] tokensParent =
                    Arrays.copyOf(tokensChild, tokensChild.length - 1);
            String parentPath = TextUtils.join(".", tokensParent);
            parentPath = parentPath.substring(0, parentPath.length());
            return parentPath;
        }
        return "";
    }

    @Override
    public String toString() {
        return getClazz().getSimpleName() + "{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", path=" + path +
                ", parent=" + parent +
                '}';
    }

    protected abstract Class<T> getClazz();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseTree be = (BaseTree) o;

        return !(getId() != null ? !getId().equals(be.getId()) : be.getId() != null);
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

    public void setChildren(List<T> children) {
        this.children = children;
    }

    public void setParent(T parent) {
        this.parent = parent;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

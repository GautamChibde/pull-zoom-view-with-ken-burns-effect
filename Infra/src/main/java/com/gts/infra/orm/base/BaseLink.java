package com.gts.infra.orm.base;

import com.gts.infra.orm.record.Record;
import com.gts.infra.orm.repo.BaseRepo2;

public abstract class BaseLink implements Record {

    private static final Long serialVersionUID = 1L;

    public void save() {
        BaseRepo2.save(this);
    }

    public boolean isArchived() {
        return false;
    }

    public boolean isNew() {
        return false;
    }

    public boolean isMutated() {
        return false;
    }

    public boolean isActive() {
        return false;
    }

    public boolean isValid() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseLink be = (BaseLink) o;

        return !(getId() != null ? !getId().equals(be.getId()) : be.getId() != null);

    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

}

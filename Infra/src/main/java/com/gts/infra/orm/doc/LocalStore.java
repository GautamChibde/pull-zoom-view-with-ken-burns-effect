package com.gts.infra.orm.doc;

import android.os.AsyncTask;

import com.gts.infra.App;
import com.gts.infra.orm.SugarRecord;
import com.gts.infra.orm.record.Record;

public class LocalStore {

    private final App app;

    public LocalStore(App app) {
        this.app = app;
    }

    public <T extends Record> void save(T record) {
        new DataTask(new SaveRequest<>(record));
    }

    public interface DataRequest {
    }

    public static class SaveRequest<T extends Record> implements DataRequest {
        private final T record;

        public SaveRequest(T record) {
            this.record = record;
        }

        public T getRecord() {
            return record;
        }
    }


    private class DataTask extends AsyncTask<Void, Void, Void> {

        private final DataRequest request;

        public DataTask(DataRequest request) {
            this.request = request;
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (request instanceof SaveRequest) {
                SugarRecord.save(((SaveRequest) request).getRecord());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (request instanceof SaveRequest) {
                app.getBus().post(
                        new RecordSavedEvent<>(((SaveRequest) request).getRecord())
                );
            }
        }
    }

    public static class RecordSavedEvent<T extends Record> {
        private final T record;

        public RecordSavedEvent(T record) {
            this.record = record;
        }

        public T getRecord() {
            return record;
        }
    }
}

package com.gts.infra.orm.repo;

import com.gts.infra.orm.base.BaseEntity;
import com.gts.infra.orm.util.NamingHelper;

import java.util.Date;
import java.util.List;

public class EntityRepo extends BaseRepo2 {

    public static <T extends BaseEntity> void archive(T entity) {
        entity.setState("archived");
        save(entity);
    }

    public static <T extends BaseEntity> T getLastUpdated(Class<T> clazz) {
        List<T> found = searchWithQuery(clazz,
                "select * from " + NamingHelper.toSQLName(clazz) +
                        " order by updated_stamp desc limit 1");
        if (found.size() == 1) {
            return found.get(0);
        } else {
            try {
                return clazz.newInstance();
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static <T extends BaseEntity> Date getLastUpdatedStamp(Class<T> clazz) {
        return getLastUpdated(clazz).getUpdatedStamp();
    }

    public static <T extends BaseEntity>
    List<T> getForCategory(Class<T> clazz, long categoryId) {
        return search(clazz, "category_id = ?", String.valueOf(categoryId));
    }
}

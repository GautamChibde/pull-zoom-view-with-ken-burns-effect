package com.gts.infra.auth;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;
import com.gts.infra.App;
import com.gts.infra.net.http.Resource;
import com.gts.util.Constant;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class AuthManager {

    private final App app;

    public AuthManager(App app) {
        this.app = app;
    }

    @SuppressWarnings("unused")
    public void onEvent(Resource.HttpRequestComplete<?> event) {
        if (Constant.AUTH_REQUEST_CODE == event.getRequestId()) {
            app.unRegisterListener(this);
            if (null != event.getResponse()
                    && event.getResponse() instanceof Auth) {
                onSuccess((Auth) event.getResponse());
            } else {
                onFailure(event.getMessage());
            }
        }
    }

    protected void onSuccess(Auth auth) {
        if (!TextUtils.isEmpty(auth.getAccess_token())) {
            Map<String, Object> credentials = new HashMap<>();
            credentials.put(Constant.KEY_USER_TOKEN, auth.getAccess_token());
            credentials.put(Constant.KEY_USER_ROL, auth.getRol());
            credentials.put(Constant.KEY_USER_ID, auth.getUser_id());
            app.saveToPref(credentials);
        } else {
            onFailure("Empty access token");
        }
    }

    protected void onFailure(String message) {
    }

    public void signUp(String name, String userName, String password) {
        sendAuthRequest(
                getAuth(name,
                        userName,
                        password,
                        Constant.GRANT_TYPE_CLIENT_CREDENTIALS
                ),
                Constant.CONTEXT_SIGNUP,
                new TypeToken<Auth>() {
                }.getType()
        );
    }

    public void login(String userName, String password) {
        sendAuthRequest(
                getAuth(null,
                        userName,
                        password,
                        Constant.GRANT_TYPE_PASSWORD
                ),
                Constant.CONTEXT_TOKEN,
                new TypeToken<Auth>() {
                }.getType()
        );
    }

    protected void sendAuthRequest(Auth authRequest, String context, Type type) {
        Resource.getUserToken(app, context, authRequest, type);
    }

    @NonNull
    protected Auth getAuth(String name, String userName,
                           String password, String grantType) {
        String[] appCredentials = app.getAppCredentials();
        return new Auth(
                appCredentials[0],
                appCredentials[1],
                name,
                null,
                userName,
                password,
                grantType
        );
    }
}
